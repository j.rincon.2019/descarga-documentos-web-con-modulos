from robot import Robot

class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url)
            robot.retrieve()
            self.cache[url] = robot.content()

    def content(self, url):
        self.retrieve(url)
        return self.cache[url]

    def show(self, url):
        content = self.content(url)
        if content is not None:
            print(content)
        else:
            print("El documento no ha sido descargado aún.")

    def show_all(self):
        for url, content in self.cache.items():
            print(url)

if __name__ == '__main__':
    print("Prueba clase Cache")
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.show('http://gsyc.urjc.es/')
    c.show_all()
