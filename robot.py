import urllib.request

class Robot:
    def __init__(self, url):
        self.url = url
        self.retrieved = False
        self.content_data = None
        print(self.url)

    def retrieve(self):
        if not self.retrieved:
            print("Descargando", self.url)
            with urllib.request.urlopen(self.url) as response:
                self.content_data = response.read().decode('utf-8')
            self.retrieved = True
        else:
            print("El contenido ya ha sido descargado anteriormente.")

    def content(self):
        self.retrieve()
        return self.content_data

    def show(self):
        print(self.content())

if __name__ == '__main__':
    print("Test Robot class")
    r = Robot('http://gsyc.urjc.es/')
    print(r.url)
    r.show()
    r.retrieve()
    r.retrieve()
