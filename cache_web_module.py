from robot import Robot
from cache import Cache

if __name__ == '__main__':
    print("PRUEBA EJECUCIÓN DEL PROGRAMA")
    print("ROBOT")
    r1 = Robot('https://www.aulavirtual.urjc.es/')
    r1.show()

    print("ROBOT")
    r2 = Robot('https://www.urjc.es/')
    r2.retrieve()
    r2.show()

    c = Cache()
    print("CACHE")
    c.retrieve('https://gitlab.eif.urjc.es/')

    print("CACHE")
    c.show('http://gsyc.urjc.es/')
    c.show('https://labs.eif.urjc.es/')

    print("CACHE")
    c.show_all()
